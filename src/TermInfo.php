<?php

namespace z0x\Pnano;

Class TermInfo
{
	public $max_width;
	public $max_height;
	public $content_width;
	public $content_height;

	function __construct(){
		$this->set_xy();
	}

	private function detect_tput(){   //roll this into some kind of error checking later
		$shellout=trim(`which tput`); // this doesn't actually work.

		if (!strlen($shellout)){
			echo "tput not detected. please install ncurses or obtain a system capable of running it.";
		return false;
		}
		return true;
	}

	private function detect_max_width(){
    	$max_width = intval(trim(`tput cols`));
    	return $max_width;
	}

	private function detect_max_height(){
		$max_height = intval(trim(`tput lines`));
		return $max_height;
	}

	public function set_xy(){
		$this->max_height = $this->detect_max_height();
		$this->max_width = $this->detect_max_width();

		$this->content_width =  $this->max_width - 2; //minus one for each char of border
		$this->content_height = $this->max_height - 3; //same as above minus one more for the input line
		
		return null;
	}
}