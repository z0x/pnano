<?php

namespace z0x\Pnano;

class PnanoIO extends NcursesGetKey{
    public $input = [0=>[]];
    public $output;
    public $file_read =false;  //we get stuck reading a file over and over again unless we set a flag.
    public $file_exists = false;
    public $file_readable = false;
    public $file_writeable = false;
    public $file_not_dir = false;
    public $readonly = false;
    public $filename;
    private $writeout="";
    public $debug_mode = false;
    private $readonly_alert_sent = false;     //i hate this
    private $title = "pnano alpha";


    function __construct()
    {
        $this->terminfo = new TermInfo();
    }

    public function startup($arg = null){
        $this->read_file_or_str_or_socket_or_w_e($arg);
        $this->set_title();
        $this->file_read=true;
        $this->ncurses_init();
        $this->echo_input_buffer();
    }
    public function shutdown(){
        ncurses_end();
        `reset && clear`;
        if($this->debug_mode) {
            var_dump($this->input);
        }
    }

    public function echo_input_buffer(){

        $this->reticulate_siplines();
        $this->translate_newlines();
        ncurses_clear();
        foreach ($this->output as $arr=> $value) {  // its way easier and faster to just
            foreach ($value as $char)               // foreach through
                ncurses_addch($char);               // and "print" the chars one by one
        }
        $this->cursor_correction();
        ncurses_move($this->cursor_y, $this->cursor_x);
        ncurses_refresh();                          // because we don't flush the output till here
    }

    public function alert($str){                   // dead simple method to deliver messages to user.
        $this->shutdown();                         // this is awful.
        echo "###\n$str\n###";                     // i kind of love it.
        sleep(5);
        $this->startup();
    }

    private function set_title(){

        /*if(isset($this->filename)
            AND !$this->file_read) {
            $this->title .=  $this->filename;
        }*/ //bah.
       // `echo -en "\033]0;$this->title\a" && export PS1=$this->title`; trying to set the terminal title here but just will not work.
        cli_set_process_title($this->title);
    }

    private function translate_newlines()           // turn 13's [return] into a series a spaces. the illusion of newlines.
    {
        $this->output = $this->input;
        $line_count = 0;

        foreach ($this->output as $arr) {
            if(!is_null($arr)) {
                $key = array_search(13, $arr);                                             // get the position of the newline char
                $pad = [];
                if ($key !== false) {   //because array_search can return a result that looks false-y  
                    $arr[$key] = 32;                                                       // overwrite newline char with space
                    $pad = array_pad($pad, $this->terminfo->max_width - sizeof($arr), 32); // create an array of spaces
                    array_splice($arr, $key, 0, $pad);                                     // then shove that fucker in real good.
                    $this->output[$line_count] = array_values($arr);                       // i just like to sprinkle this around some times.
                    $this->output[$line_count] = array_pad($this->output[$line_count], $this->terminfo->max_width, 32); //32 = space
                }                                                                          // ^ i'm honestly going to have to look at this in a debugger to figure out what i did..
            }
            $line_count++;
        }



    }

    private function reticulate_siplines()    {
        $holder_arr = [];
        $new_arr = [];
        $flat = [];   //yes we need this.
        $flat = call_user_func_array('array_merge', $this->input);  // flatten the multidimentional input
        $count = 1; //we're comparing sizeof();
        foreach ($flat as $value) {                                 // loop through the flat array,
            array_push($holder_arr, $value);                        // pushing elements into a temp array
            if (sizeof($holder_arr) === $this->terminfo->max_width  // until it is the size we're looking for
                OR $value === 13                                    // or is a newline
                OR $count === sizeof($flat)                         // or is the last element in the flat array
            ) {
                array_push($new_arr, $holder_arr);                  //then push the temp array into (what will be) new $input array
                $holder_arr = [];                                   // and clear the temp array for the next line
                if($count === sizeof($flat)
                    AND isset($this->input[sizeof($this->input) -1])
                    AND $this->input[sizeof($this->input) -1] === []// and if we've got a empty array dangling at the and
                ) {
                    array_push($new_arr, []);                       // preserve it.
                }
            }
            $count++;
        }

        if(sizeof($new_arr) >= $this->terminfo->max_height   ///very quick and dirty to be as non-destructive as possible.
            AND !$this->readonly_alert_sent) {
            $this->readonly = true;
            $this->readonly_alert_sent =true;
            $this->alert('Current behavior undefined. Entering Readonly mode.');
        }
            $this->input = $new_arr;

    }


    private function str_to_buffer($str){
        $arr = str_split($str);
        $this->input = [];
        array_push($this->input, $this->xlate_ord($arr));
    }

    private function file_to_buffer($argv){
        $this->filename = getcwd() . "/" . end($argv);
        if(!$this->filename){
            $this->filename = getcwd() . "/pnano.out";
        }
        $this->finfo($this->filename);
        if($this->file_exists
            AND $this->file_readable
            AND $this->file_not_dir){
                $str = file_get_contents($this->filename);
                $this->str_to_buffer($str);
        }
          //i just realized i have literally no error checking.
    }
    
    public function buffer_to_file($input){
        $this->xlate_char($input);
        $fh = fopen($this->filename, "w+");
        fwrite($fh,$this->writeout);
        fclose($fh);
    }

    private function xlate_char($input){   //turn the array of not-quite-ascii $input ints to corresponding string
        foreach ($input as $arr){
            foreach ($arr as $value){
                $this->writeout.=chr($value);
            }
        }
    }

    private function finfo($filename){
        if(file_exists($filename)) $this->file_exists = true;
        if(is_readable($filename)) $this->file_readable = true;
        if(is_writeable($filename)) $this->file_writeable = true;
        if(is_file($filename)) $this->file_not_dir = true;
    }

    private function xlate_ord($arr){   //ord to char
        $ord_arr=[];
        foreach ($arr as $value){
            if($value === "\n"){
                array_push($ord_arr, 13);
            }
            else{
                array_push($ord_arr,ord($value));
            }
        }
        return $ord_arr;
    }
 
    private function read_file_or_str_or_socket_or_w_e($arg){
        if(!$this->file_read){
                switch ($arg){
                    case is_array($arg) AND in_array('-d',$arg) :
                        $this->debug_mode = true;  // and don't break. You'd think this would be the place to use 'continue', but you'd be wrong
                    case is_array($arg) AND sizeof($arg) > 1 :  // looking for a filename in end(argv)
                        $this->file_to_buffer($arg);
                        break;
                    case is_string($arg):
                        $this->str_to_buffer($arg);
                        break;
                    default:
                        break;
                }
        }
    }

    private function ncurses_init(){ //IDGAF
        ncurses_init();
        ncurses_start_color();
        ncurses_noecho();
        ncurses_noqiflush();
        ncurses_nonl();
        ncurses_refresh();
    }
}