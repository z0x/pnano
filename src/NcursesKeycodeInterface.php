<?php

namespace z0x\Pnano;

interface NcursesKeycodeInterface {
    public function key_return();
    public function key_up();
    public function key_down();
    public function key_left();
    public function key_right();
    public function key_backspace();
    public function key_delete();
    public function key_home();
    public function key_end();
    public function key_pgup();
    public function key_pgdn();
    public function key_tab();
    public function key_char();
}