<?php
namespace z0x\Pnano;

class NcursesKeycode extends NcursesCursor implements NcursesKeycodeInterface 
{
    public function key_return()
    {
        if ($this->insert_mode) {
            array_splice($this->input[$this->cursor_y], $this->cursor_x, 0, [13]);
        } else {
            array_push($this->input[$this->cursor_y], 13);
        }
        $this->cursor_x = 0;
        $this->cursor_y++;
        if (!isset($this->input[$this->cursor_y])) {
            array_push($this->input, []);
        }
    }

    public function key_up()
    {
        if ($this->can_move_up) {
            $this->cursor_y--;
        } elseif ($this->can_jump_up) {
            $this->cursor_y--;
            end($this->input[$this->cursor_y]);
            $this->cursor_x = key($this->input[$this->cursor_y]);
        }
    }

    public function key_down()
    {
        if ($this->can_move_down) {
            $this->cursor_y++;
        } elseif ($this->can_jump_down ) {
            $prev = sizeof($this->input[$this->cursor_y]);
            $this->cursor_y++;
            $this->find_line_size();
            $next = sizeof($this->input[$this->cursor_y]);
            if ($prev > $next AND $this->cursor_x > $next) {
                $this->cursor_x = $this->line_size;
            }
        }
    }

    public function key_left()
    {
        if ($this->can_move_left) {
            $this->cursor_x--;
        } elseif ($this->can_jump_up) {
            $this->cursor_y--;
            $this->cursor_x = sizeof($this->input[$this->cursor_y]) - 1;
            error_log("yup");
        }
    }

    public function key_right()
    {
        if ($this->can_move_right) {
            $this->cursor_x++;
        } elseif ($this->can_jump_down) {
            $this->cursor_x = 0;
            $this->cursor_y++;
        }

    }

    public function key_backspace()
    {
        if ($this->can_move_left === true) { //really?
            if(!$this->can_move_right
                AND $this->nl_eol){
                unset($this->input[$this->cursor_y][$this->cursor_x]);
            }
            $this->cursor_x--;
            if ($this->insert_mode) {
                unset($this->input[$this->cursor_y][$this->cursor_x]);
            } else {
                if (!empty($this->input[$this->cursor_y])) {
                    array_pop($this->input[$this->cursor_y]);
                }
            }
        }
        elseif ($this->can_jump_up
            AND $this->cursor_x === 0) {
                $this->cursor_y--;
                $this->find_line_size();
                unset($this->input[$this->cursor_y][$this->line_size -1 ]);
                $this->find_line_size();
                $this->cursor_x = $this->line_size -1 ;
        }
    }

    public function key_delete()
    {
        if ($this->can_move_right) {
            unset($this->input[$this->cursor_y][$this->cursor_x]);
            $this->input[$this->cursor_y] = array_values($this->input[$this->cursor_y]);
        }
        if($this->can_move_up
            AND empty($this->input[$this->cursor_y])
            ){
                unset($this->input[$this->cursor_y]);
                $this->cursor_y--;
                $this->find_line_size();
                $this->cursor_x = $this->line_size;
        }
    }

    public function key_home()
    {
        if ($this->can_move_left) {
            $this->cursor_x = 0;
        }
    }

    public function key_end()
    {
        if ($this->can_move_right) {
            if (sizeof($this->input[$this->cursor_y]) === $this->terminfo->max_width) {
                $this->cursor_x = sizeof($this->input[$this->cursor_y]) - 1;  //set it to the end up the line, instead o jumping up one;
            } else {
                $this->cursor_x = sizeof($this->input[$this->cursor_y]);
            }
        }
    }

    public function key_pgup()
    {
        if ($this->can_move_up) {
            if ($this->cursor_y <= 9) {
                $this->cursor_y = 0;
            } else $this->cursor_x = $this->cursor_x - 10; //todo: eliminate magic numbers
        }
    }

    public function key_pgdn()
    {
        if($this->can_move_down){
            if($this->cursor_y - (sizeof($this->input) -1 )<= 0 ){
                $this->cursor_y = sizeof($this->input) -1;
            } else{
                $this->cursor_y = $this->cursor_y + 10;
            }
        }
    }

    public function key_tab()
    {   //indent with spaces
        if ($this->insert_mode) {
            for ($i = 0; $i < 4; $i++) {
                array_splice($this->input[$this->cursor_y], $this->cursor_x, 0, [32]); //32 = space
                $this->input = array_values($this->input);
                $this->cursor_x++;
            }
        } else {
            for ($i = 0; $i < 4; $i++) {
                array_push($this->input[$this->cursor_y], 32); //32 = space
                $this->cursor_x++;
            }
        }
    }
    

    public function key_char()
    {
        if ($this->insert_mode) {
            array_splice($this->input[$this->cursor_y], $this->cursor_x, 0, [$this->key]);
        } else {
            array_push($this->input[$this->cursor_y], $this->key);
        }
        $this->cursor_x++;
    }
    
}