<?php
namespace z0x\Pnano;

class NcursesCursor
{
    public $can_move_up = false;    //  has an input[element] directly above it
    public $can_move_down = false;  //  has an input[element] directly above it
    public $can_move_left = false;
    public $can_move_right = false;
    public $can_jump_up = false;    // true as long as not the top line
    public $can_jump_down = false;  // true as long as not bottom line and sizeof input > 1
    public $cursor_x = 0;
    public $cursor_y = 0;
    public $insert_mode = false;
    public $input;
    public $terminfo;
    public $key;
    public $line_size;
    public $nl_eol = false;

    public function cursor_allowed_moves()
    {
        $this->find_line_size();

        $this->cursor_allowed_down();
        $this->cursor_allowed_up();
        $this->cursor_allowed_left();
        $this->cursor_allowed_right();
    }

    private function cursor_allowed_down()
    {

        if ($this->cursor_y < (sizeof($this->input) - 1)) {
            $this->can_move_down = true;
            $this->can_jump_down = true;
        }
        if ($this->cursor_y === (sizeof($this->input) - 1)) {
            $this->can_jump_down = false;
        }
        if (!isset($this->input[$this->cursor_y + 1][$this->cursor_x])) {
            $this->can_move_down = false;
        }
        if ($this->cursor_y === sizeof($this->input) - 1) {
            $this->can_move_down = false;
            $this->can_jump_down = false;
        }
    }

    private function cursor_allowed_up()
    {
        if ($this->cursor_y < sizeof($this->input)) {
            $this->can_move_up = true;
        }
        if ($this->cursor_y <= sizeof($this->input) - 1
            and $this->cursor_y !== 0
        ) {
            $this->can_jump_up = true;
        }
        if (!isset($this->input[$this->cursor_y - 1][$this->cursor_x])) {
            $this->can_move_up = false;
        }
        if ($this->cursor_y === 0) {
            $this->can_move_up = false;
            $this->can_jump_up = false;
        }
    }

    private function cursor_allowed_left()
    {

        if ($this->cursor_x > 0) {
            $this->can_move_left = true;
        }
        if($this->cursor_x === 0){
            $this->can_move_left = false;
        }

        if ($this->cursor_x === 0 && $this->can_move_up === false) {
            $this->can_move_left = false;
        }

    }

    private function cursor_allowed_right()
    {
        if ($this->cursor_x === $this->line_size ) {
            if($this->nl_eol){
                $this->insert_mode = true;
            }else $this->insert_mode = false;
            $this->can_move_right = false;
        }

        if (isset($this->input[$this->cursor_y][$this->cursor_x + 1]) /*AND $this->input[$this->cursor_y][$this->cursor_x + 1] === 13*/) {
            $this->can_move_right = false;
        }

        if ($this->cursor_x < $this->line_size) {
            $this->insert_mode = true;
            $this->can_move_right = true;
        } else {
            $this->can_move_right = false;
        }

    }

    public function find_line_size()
    {
        $this->line_size = sizeof($this->input[$this->cursor_y])  ;

        if (isset($this->input[$this->cursor_y][$this->line_size - 1])
                AND $this->input[$this->cursor_y][$this->line_size - 1] === 13) {
            $this->line_size--;
            $this->nl_eol = true;
        } else {
            $this->nl_eol = false;
        }
        if($this->line_size === -1){
            $this->line_size = 0;
        }
    }

    public function cursor_correction(){

        if($this->cursor_x >= ($this->terminfo->max_width )){   // if the x value overflows the line length
            $this->cursor_x = 0 ;                               // go to the start
            $this->cursor_y++;                                  // of the next line
        }

        if($this->cursor_x < 0                                  // if the x value underflows
            AND $this->can_jump_up ){                            // and there's a line above
            $this->cursor_y--;                                  // go to the end of line above
            $this->find_line_size();
            $this->cursor_x = $this->line_size -1;
        }

        if(!isset($this->input[$this->cursor_y])){              // if we find ourselves at a y-position w/o
            $this->input[$this->cursor_y] = [];                 // a corresponding key in input, create one
        }
    }

}