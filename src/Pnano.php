<?php
namespace z0x\Pnano;

class Pnano extends PnanoIO
{
 
    public function pnano($arg = null){
        $this->startup($arg);

        while(true) {
            $this->key = $this->getch_nonblock(1000000);

            if(is_null($this->key)) {
                continue;
            }else{
                switch ($this->key) {

                    case 13: //return
                        $this->key_return();
                        break;

                    case 27: // esc
                        //$this->input = $input;
                        break 2; //exits

                    case 263: //backspace
                        $this->key_backspace();
                        break;

                    case 330: //delete
                        $this->key_delete();

                        break;

                    case NCURSES_KEY_DC;
                        break;
                    
                    case 262: //home
                        $this->key_home();
                        break;

                    case NCURSES_KEY_END: // 369: //end
                        $this->key_end();
                        break;

                    case 339: //pgup
                        $this->key_pgup();
                        break;

                    case 338: //pgdn
                        $this->key_pgdn();
                        break;

                    case 9: //tab
                        $this->key_tab();
                        break;

                    case NCURSES_KEY_UP:
                        $this->key_up();
                        break;

                    case NCURSES_KEY_DOWN:
                        $this->key_down();
                        break;

                    case NCURSES_KEY_LEFT:
                        $this->key_left();
                        break;

                    case NCURSES_KEY_RIGHT:
                        $this->key_right();
                        break;
                    
                    case 15:    //crtl + o   write out
                        if(!$this->readonly){
                            $this->buffer_to_file($this->input); //this 
                        } else {
                            $this->alert("Writing a file taller than the terminal is currently undefined behavior.\nScrolling support coming soon.\nCheck https://gitlab.com/z0x/pnano for updates\n");
                        }
                        break;
                    case NCURSES_KEY_COMMAND:
                        //was trying to detect control key here.
                        //current version of ncurses-php doesn't seem to properly handle meta codes
                        break;

                    default:
                        $this->key_char();
                        break;
                }
            }
            $this->echo_input_buffer();
            $this->cursor_allowed_moves();

            if($this->debug_mode){
                error_log("Insert Mode " . $this->insert_mode);
                error_log($this->cursor_y.", ".$this->cursor_x);
                error_log(sizeof($this->input[$this->cursor_y]));
                error_log("LEFT\t".$this->can_move_left);
                error_log("RIGHT\t". $this->can_move_right);
                error_log("UP\t". $this->can_move_up);
                error_log("DOWN\t". $this->can_move_down);
                error_log("JUMP UP\t  $this->can_jump_up");
                error_log("JUMP DOWN\t  $this->can_jump_down");
            }
        }
        
        $this->shutdown();
    }
}

/*NCURSES_KEY_DOWN
down arrow
NCURSES_KEY_UP
up arrow
NCURSES_KEY_LEFT
left arrow
NCURSES_KEY_RIGHT
right arrow
NCURSES_KEY_HOME
home key (upward+left arrow)
NCURSES_KEY_BACKSPACE
backspace
NCURSES_KEY_DL
delete line
NCURSES_KEY_IL
insert line
NCURSES_KEY_DC
delete character
NCURSES_KEY_IC
insert char or enter insert mode
NCURSES_KEY_EIC
exit insert char mode
NCURSES_KEY_CLEAR
clear screen
NCURSES_KEY_EOS
clear to end of screen
NCURSES_KEY_EOL
clear to end of line
NCURSES_KEY_SF
scroll one line forward
NCURSES_KEY_SR
scroll one line backward
NCURSES_KEY_NPAGE
next page
NCURSES_KEY_PPAGE
previous page
NCURSES_KEY_STAB
set tab
NCURSES_KEY_CTAB
clear tab
NCURSES_KEY_CATAB
clear all tabs
NCURSES_KEY_SRESET
soft (partial) reset
NCURSES_KEY_RESET
reset or hard reset
NCURSES_KEY_PRINT
print
NCURSES_KEY_LL
lower left
NCURSES_KEY_A1
upper left of keypad
NCURSES_KEY_A3
upper right of keypad
NCURSES_KEY_B2
center of keypad
NCURSES_KEY_C1
lower left of keypad
NCURSES_KEY_C3
lower right of keypad
NCURSES_KEY_BTAB
back tab
NCURSES_KEY_BEG
beginning
NCURSES_KEY_CANCEL
cancel
NCURSES_KEY_CLOSE
close
NCURSES_KEY_COMMAND
cmd (command)
NCURSES_KEY_COPY
copy
NCURSES_KEY_CREATE
create
NCURSES_KEY_END
end
NCURSES_KEY_EXIT
exit
NCURSES_KEY_FIND
find
NCURSES_KEY_HELP
help
NCURSES_KEY_MARK
mark
NCURSES_KEY_MESSAGE
message
NCURSES_KEY_MOVE
move
NCURSES_KEY_NEXT
next
NCURSES_KEY_OPEN
open
NCURSES_KEY_OPTIONS
options
NCURSES_KEY_PREVIOUS
previous
NCURSES_KEY_REDO
redo
NCURSES_KEY_REFERENCE
ref (reference)
NCURSES_KEY_REFRESH
refresh
NCURSES_KEY_REPLACE
replace
NCURSES_KEY_RESTART
restart
NCURSES_KEY_RESUME
resume
NCURSES_KEY_SAVE
save
NCURSES_KEY_SBEG
shiftet beg (beginning)
NCURSES_KEY_SCANCEL
shifted cancel
NCURSES_KEY_SCOMMAND
shifted command
NCURSES_KEY_SCOPY
shifted copy
NCURSES_KEY_SCREATE
shifted create
NCURSES_KEY_SDC
shifted delete char
NCURSES_KEY_SDL
shifted delete line
NCURSES_KEY_SELECT
select
NCURSES_KEY_SEND
shifted end
NCURSES_KEY_SEOL
shifted end of line
NCURSES_KEY_SEXIT
shifted exit
NCURSES_KEY_SFIND
shifted find
NCURSES_KEY_SHELP
shifted help
NCURSES_KEY_SHOME
shifted home
NCURSES_KEY_SIC
shifted input
NCURSES_KEY_SLEFT
shifted left arrow
NCURSES_KEY_SMESSAGE
shifted message
NCURSES_KEY_SMOVE
shifted move
NCURSES_KEY_SNEXT
shifted next
NCURSES_KEY_SOPTIONS
shifted options
NCURSES_KEY_SPREVIOUS
shifted previous
NCURSES_KEY_SPRINT
shifted print
NCURSES_KEY_SREDO
shifted redo
NCURSES_KEY_SREPLACE
shifted replace
NCURSES_KEY_SRIGHT
shifted right arrow
NCURSES_KEY_SRSUME
shifted resume
NCURSES_KEY_SSAVE
shifted save
NCURSES_KEY_SSUSPEND
shifted suspend
NCURSES_KEY_UNDO
undo
NCURSES_KEY_MOUSE
mouse event has occurred
NCURSES_KEY_MAX
maximum key value*/