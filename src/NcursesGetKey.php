<?php

namespace z0x\Pnano;

class NcursesGetKey extends NcursesKeycode{

    public function getch_nonblock($timeout) { /// ripped straight out of of the php manual notes
        $read = array(STDIN);
        $null = null;    // stream_select() uses references, thus variables are necessary for the first 3 parameters
        if(stream_select($read,$null,$null,floor($timeout / 1000000),$timeout % 1000000) != 1) return null;
        return ncurses_getch();
    }

}