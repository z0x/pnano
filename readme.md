# pnano
### an ncurses based text editor written in php

![hello world](media/example0.gif)

#### Usage:
    ./pnano.php [-d] [filename]

or

    $app = new Pnano();
    $app->pnano($string);

#### Commands
`ctrl+o` Write to file. (default pnano.out in cwd)

`esc` exit gracefully.
#### Flags:
`-d` Debug

#### Requirements:
Php 7 +

[This](https://bugs.php.net/patch-display.php?bug_id=71299&patch=ncurses-php7-support-again.patch&revision=latest) patch for ncurses-php.

#### Building Ncurses From Source for php7+

* Apply [This](https://bugs.php.net/patch-display.php?bug_id=71299&patch=ncurses-php7-support-again.patch&revision=latest) patch to the latest [ncurses extension source](https://github.com/nickl-/pecl-ncurses) .

    `git am --signoff < ncurses-php7-support-again.patch`
* Build and install as you would any other php extension.
    * You'll need all the relevant dev packages for your system.
    * Don't forget to add `extension=ncurses.so` to your php cli .ini

     ```
     phpize
     ./configure
     make
     make test
     sudo make install
     ```

* Verify installation with `php -m | grep ncurses`

#### Notice
Files in the media folder, and anything not covered by GPLv2, are public domain.